<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<p />

<div class="table-responsive">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Offer</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="offer" items="${offers}">

				<tr class="offerRow">
					<td class="name"><c:out value="${offer.user.name}" /></td>
					<td class="contact"><a
						href="<c:url value='/message?uid=${offer.username}'/>">contact</a>
					</td>
					<td class="offer"><c:out value="${offer.text}"></c:out></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>




