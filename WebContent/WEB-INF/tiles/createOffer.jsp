<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<script type="text/javascript">
	function onDeleteClick() {
		var doDelete = confirm("Are you sure you want to dlete ths offer?");
		if (doDelete == false) {
			event.preventDefault();
		}
	}
	function onReady() {
		//alert("Hello");
		$("#delete").click(onDeleteClick);
	}
	$(document).ready(onReady);
</script>
<div class="row">
	<div class="col-md-5">
		<sf:form method="post"
			action="${pageContext.request.contextPath}/doCreate"
			commandName="offer">
			<sf:input type="hidden" path="id" />
			<!-- save value id -->
			<div class="form-group">
				<label>Your offer:</label>
				<sf:textarea class="form-control" path="text" rows="5"
					placeholder="Your Offer..."></sf:textarea>
				<sf:errors cssClass="error" path="text"></sf:errors>
			</div>
			<button class="btn btn-primary" type="submit">Save advert</button>
			<c:if test="${offer.id !=0}">
				<button class="btn btn-danger" name="delete" id="delete"
					type="submit">delete this offer.</button>
			</c:if>

		</sf:form>
	</div>
</div>

