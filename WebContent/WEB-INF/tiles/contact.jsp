<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<div class="row jumbotron">
	<h2>Send Message</h2>
	<sf:form method="post" commandName="message" class="form-horizontal">
		<input type="hidden" name="_flowExecutionKey"
			value="${flowExecutionKey}" />
		<!-- ???? -->
		<input type="hidden" name="_eventId" value="send" />
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Your
				name:</label>
			<div class="col-sm-10">
				<sf:input class="form-control" path="name" type="text"
					value="${fromName}" placeholder="Your name" />
				<sf:errors path="name" class="error"></sf:errors>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Your email:</label>
			<div class="col-sm-10">
				<sf:input class="form-control" path="email" type="text"
					value="${fromEmail}" placeholder="Your Email" />
				<sf:errors path="email" class="error"></sf:errors>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Subject:</label>
			<div class="col-sm-10">
				<sf:input class="form-control" path="subject" type="text"
					placeholder="subject..." />
				<sf:errors path="subject" class="error"></sf:errors>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Your Message:</label>
			<div class="col-sm-10">
				<sf:textarea class="form-control" path="content" rows="5" type="text"
					placeholder="messsage..." />
				<sf:errors path="content" class="error"></sf:errors>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary">Send message</button>
			</div>
		</div>
	</sf:form>
</div>



