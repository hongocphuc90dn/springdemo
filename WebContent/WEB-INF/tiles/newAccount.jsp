<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<sf:form id="details" class="form-horizontal" method="post" encType="multipart/form-data"
	action="${pageContext.request.contextPath}/createAccount"
	modelAttribute="user">
	<div class="form-group">
		<label class="col-sm-2 control-label">Username:</label>
		<div class="col-sm-10">
			<sf:input class="form-control" path="username" type="text"
				placeholder="username" />
			<div class="error">
				<sf:errors path="username"></sf:errors>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Name:</label>
		<div class="col-sm-10">
			<sf:input class="form-control" path="name" type="text"
				placeholder="your name" />
			<div class="error">
				<sf:errors path="name"></sf:errors>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Email:</label>
		<div class="col-sm-10">
			<sf:input class="form-control" path="email" type="email"
				placeholder="example@mail.com" />
			<div class="error">
				<sf:errors path="email"></sf:errors>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Password:</label>
		<div class="col-sm-10">
			<sf:input class="form-control" path="password" type="password"
				id="password" />
			<div class="error">
				<sf:errors path="password"></sf:errors>
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Confirm Password:</label>
		<div class="col-sm-10">
			<input class="form-control" name="confirmPass" type="password"
				id="confirmPass" />
			<div id="matchPass"></div>
		</div>

	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Avatar:</label>
		<div class="col-sm-10">
			<input type="file" name="avatars[0]" value="Select image ..."
				onchange="readURL(this);" /> <img
				src="https://s3-ap-southeast-1.amazonaws.com/phucbucket/images/avatar/default_avatar.png"
				class="avatar img-thumbnail" alt="Avatar" width="250" height="250">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-primary">Create account</button>
		</div>
	</div>
</sf:form>
<script type="text/javascript">
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('.avatar').attr('src', e.target.result);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}
	$('#i_file').change( function(event) {
    var tmppath = URL.createObjectURL(event.target.files[0]);
    $("img").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));

    $("#disp_tmp_path").html("Temporary Path(Copy it and try pasting it in browser address bar) --> <strong>["+tmppath+"]</strong>");
});
</script>


