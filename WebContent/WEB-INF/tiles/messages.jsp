<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<div id="messages"></div>
<div class="message template hidden">
	<div class="subject">_subject</div>
	<div class="messageBody">_body</div>
	<div>
		<span class="name">_name</span>( <a class="replylink" href="#">_email</a>)
	</div>
	<div class="messageSent alert alert-success" style="display: none">Message
		sent.</div>
	<form class="replyForm" style="display: none;">
		<div class="form-group">
			<textarea class="replyarea form-control"></textarea>
		</div>
		<!-- <input class="replyButton" type="button" value="reply"> -->
		<div class="form-group">
			<button type="submit" class="replyButton btn btn-primary btn-lg"
				id="load"
				data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> is sending email...">Reply</button>
		</div>
	</form>
</div>
<script type="text/javascript">
	var timer;
	function startTimer() {
		timer = window.setInterval(updatePage, 500000);
	}
	function stopTimer() {
		window.clearInterval(timer);
	}
	function replyMessage() {

	}
	//	MESSAGE_DIV	
	function createMessageDiv(message) {
		var messageClone = $(".message.hidden").clone();
		messageClone.removeClass('hidden');

		messageClone.find(".subject").text(message.subject);
		messageClone.find(".name").text(message.name);
		messageClone.find(".messageBody").text(message.content);
		messageClone.find(".replylink").text(message.email).click(function() {
			stopTimer();
			messageClone.find(".replyForm").toggle();
			if (messageClone.find(".replyForm").is(":visible")) {
				messageClone.find(".messageSent").hide();
			}
		});
		messageClone.find(".replyButton").click(function(e) {
			e.preventDefault()
			stopTimer();
			var $this = $(this);
			$this.button('loading');
			var dataSent = {
				"text" : messageClone.find(".replyarea").val(),
				"name" : messageClone.find(".name").text(),
				"email" : messageClone.find(".replylink").text()
			};
			$.ajax({
				"type" : 'POST',
				"url" : "<c:url value='/sendMessage'/>",
				"data" : JSON.stringify(dataSent),
				"success" : function(data) {
					messageClone.find(".replyForm").toggle();
					messageClone.find(".messageSent").show();
					$this.button('reset');
					startTimer();

				},
				"error" : function(data) {
					alert("Error sending message.");
				},
				contentType : "application/json",
				dataType : "json"
			});
		});

		return messageClone;
	}
	//################################################
	function showMessages(data) {
		$("div#messages").html("");
		for (var i = 0; i < data.messages.length; i++) {
			var message = data.messages[i];
			var messageDiv = createMessageDiv(message);
			$("div#messages").append(messageDiv);
		}

	}
	//##############################################
	$(document).ready(onLoad);
	function onLoad() {
		updatePage();
		startTimer();

	}
	function updatePage() {
		$.getJSON("<c:url value="/getMessages"/>", showMessages);
	}
</script>
