<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><tiles:insertAttribute name="title"></tiles:insertAttribute></title>
<link href="${pageContext.request.contextPath}/static/css/bootstrap.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/script/jquery.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/script/bootstrap.js"></script>
<tiles:insertAttribute name="includes"></tiles:insertAttribute>

</head>
<body>
	<div class="container">
		<div class="row">
			<div class="header">
				<tiles:insertAttribute name="header"></tiles:insertAttribute>
			</div>
			<div class="toolbar">
				<tiles:insertAttribute name="toolbar"></tiles:insertAttribute>
			</div>

			<div class="content">
				<tiles:insertAttribute name="content"></tiles:insertAttribute>
			</div>
			<hr />
			<div class="footer">
				<tiles:insertAttribute name="footer"></tiles:insertAttribute>
			</div>
		</div>

	</div>


</body>
</html>