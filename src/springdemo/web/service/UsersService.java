package springdemo.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import springdemo.dao.Message;
import springdemo.dao.MessagesDao;
import springdemo.dao.User;
import springdemo.dao.UsersDao;

@Service
public class UsersService {
	@Autowired
	private UsersDao usersDao;
	@Autowired
	private MessagesDao messagesDao;
	

	
	public void create(User user) {
		usersDao.create(user);
		
	}
	public User getUser(String username) {
		return usersDao.getUser(username);
	}
	public boolean exists(String username) {		
		return usersDao.exists(username);
	}

	@Secured("ROLE_ADMIN")
	public List<User> getAllUsers() {
		return usersDao.getAllUsers();
	}

	public void sendMessage(Message message){
		messagesDao.saveOrUpdate(message);;
	}
	public List<Message> getMessages(String username) {
		return messagesDao.getMessages(username);
	}
	
	
	
}
