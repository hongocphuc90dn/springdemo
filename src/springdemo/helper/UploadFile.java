package springdemo.helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.springframework.web.multipart.MultipartFile;

public class UploadFile {
	static public  List<String> uploadFiles(List<MultipartFile> files) throws IllegalStateException, IOException{

        String saveDirectory = "C:/_upload/";
        List<String> fileNames = new ArrayList<String>();
 
        if (null != files && files.size() > 0) {
            for (MultipartFile multipartFile : files) {
 
                String fileName = multipartFile.getOriginalFilename();
                if (!"".equalsIgnoreCase(fileName)) {
                    // Handle file content - multipartFile.getInputStream()
                    multipartFile
                            .transferTo(new File(saveDirectory + fileName));
                    fileNames.add(fileName);
                }
            }
        }
        return fileNames;
       
    }
	static public void uploadFile(HttpServletRequest request){
		if(!ServletFileUpload.isMultipartContent(request)){
			// Nothing uploaded
			System.out.println("Nothing uploaded");
			return;
		}
		FileItemFactory itemFactory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(itemFactory);
		
		try{
			List<FileItem> items = upload.parseRequest((RequestContext) request);
			for(FileItem item: items){
				String contentType = item.getContentType();
				
				if(!contentType.equals("image/png")){
					System.out.println("Only png format image files supported");
					continue;
				}
				
				File uploadDir = new File("C:\\_upload");
				File file = File.createTempFile("avatar", ".png", uploadDir);
				item.write(file);
				
				System.out.println(("File saved."));
			}
		}catch(FileUploadException e){
			System.out.println(("File saved."));
		}catch(Exception ex){
			System.out.println("Can't save file");
		}
	}
}
